package com.nilin.algorithm;

import java.util.HashSet;

/**
 * 给定一个包含大写字母和小写字母的字符串 s ，返回 通过这些字母构造成的 最长的回文串 。
 * 在构造过程中，请注意 区分大小写 。比如 "Aa" 不能当做一个回文字符串。
 *
 * 示例 1:
 * 输入:s = "abccccdd"
 * 输出:7
 * 解释:
 * 我们可以构造的最长的回文串是"dccaccd", 它的长度是 7。
 *
 * 示例 2:
 * 输入:s = "a"
 * 输出:1
 *
 * 示例 3：
 * 输入:s = "aaaaaccc"
 * 输出:7
 *
 * 提示:
 * 1 <= s.length <= 2000
 * s 只由小写 和/或 大写英文字母组成
 */
public class LongestPalindrome {

    public static void main(String[] args) {
        int length = longestPalindrome("hbff");
        System.out.println(length);
    }


    //https://leetcode-cn.com/problems/longest-palindrome/description/
    public static int longestPalindrome(String s) {
        if (s.length() == 0)
            return 0;
        // 用于存放字符
        HashSet<Character> hashset = new HashSet<Character>();
        char[] chars = s.toCharArray();
        int count = 0;
        for (int i = 0; i < chars.length; i++) {
            if (!hashset.contains(chars[i])) {// 如果hashset没有该字符就保存进去
                hashset.add(chars[i]);
            } else {// 如果有,就让count++（说明找到了一个成对的字符），然后把该字符移除
                hashset.remove(chars[i]);
                count++;
            }
        }
        return hashset.isEmpty() ? count * 2 : count * 2 + 1;
    }

}
