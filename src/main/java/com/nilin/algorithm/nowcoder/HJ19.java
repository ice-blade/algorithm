package com.nilin.algorithm.nowcoder;

import java.util.*;

public class HJ19 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Map<String, Integer> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String line = in.nextLine();
            String key = line.substring(line.lastIndexOf("\\") + 1);
            int index = key.indexOf(" ");
            if (index > 16) {
                key = key.substring(index - 16);
            }
            if (map.containsKey(key)) {
                map.put(key, map.get(key) + 1);
            } else {
                map.put(key, 1);
                list.add(key);
            }
        }

        for (int i = list.size() > 8 ? list.size() - 8 : 0; i < list.size(); i++) {
            String key = list.get(i);
            Integer value = map.get(key);
            System.out.println(key + " " + value);
        }
    }
}
