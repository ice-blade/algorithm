package com.nilin.algorithm.nowcoder;

import java.util.Scanner;
import java.util.regex.Pattern;

public class HJ17 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()){
            String line = in.nextLine();
            String[] arr = line.split(";");
            Pos pos = new Pos(0, 0);
            for (int i = 0;i < arr.length;i++){
                String item = arr[i];
                if(!Pattern.matches("[ADWS][0-9]+",item))
                    continue;
                cal(pos,item);
            }
            System.out.println(pos);
        }
    }

    public static void cal(Pos pos, String item){
        String start = item.substring(0, 1);
        String dis = item.substring(1);
        Integer value = Integer.valueOf(dis);
        switch (start){
            case "A":
                pos.setX(pos.getX() - value);
                break;
            case "D":
                pos.setX(pos.getX() + value);
                break;
            case "W":
                pos.setY(pos.getY() + value);
                break;
            case "S":
                pos.setY(pos.getY() - value);
                break;
            default:
                break;
        }
    }

    public static class Pos{
        private int x;
        private int y;

        public Pos(int x, int y){
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        @Override
        public String toString() {
            return x + "," + y;
        }
    }
}
