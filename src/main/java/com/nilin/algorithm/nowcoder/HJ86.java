package com.nilin.algorithm.nowcoder;

import java.util.Scanner;

public class HJ86 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int nextInt = sc.nextInt();
            String binaryString = Integer.toBinaryString(nextInt);
            char[] arr = binaryString.toCharArray();
            int length = 0;
            int stamp = 0;
            for (int i = 0; i < arr.length; i++){
                if(arr[i] == '1'){
                    stamp++;
                    if(length < stamp)
                    length = stamp;
                }else {
                    stamp = 0;
                }
            }

            System.out.println(length);
        }
    }
}