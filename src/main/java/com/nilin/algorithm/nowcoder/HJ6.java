package com.nilin.algorithm.nowcoder;

import java.util.Scanner;

public class HJ6 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) {
            int next = in.nextInt();
            StringBuilder builder = new StringBuilder();
            int n = 2;
            while (n <= Math.sqrt(next)) {
                if (0 == next % n) {
                    builder.append(n).append(" ");
                    next = next / n;
                } else {
                    n++;
                }
            }
            builder.append(next);
            System.out.println(builder);
        }
    }

}
