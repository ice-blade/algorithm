package com.nilin.algorithm.nowcoder;

import java.util.Scanner;

/**
 * 合并两个有序链表
 */
public class Test1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String a = in.nextLine();
            String b = in.nextLine();
            String[] arr1 = a.split(" ");
            String[] arr2 = b.split(" ");
            ListNode list1 = createListNode(arr1);
            ListNode list2 = createListNode(arr2);
            ListNode list = mergeListNode(list1, list2);
            while (list != null) {
                System.out.print(list.val + " ");
                list = list.next;
            }
        }
    }

    static ListNode mergeListNode(ListNode list1, ListNode list2){

        ListNode pre = new ListNode(0);
        ListNode node = pre;
        while (list1 != null && list2 != null){
            if(list1.val < list2.val){
                node.next = list1;
                node = node.next;
                list1 = list1.next;
            }else {
                node.next = list2;
                node = node.next;
                list2 = list2.next;
            }
        }
        if(list2 != null){
            node.next = list2;
        }
        if(list1 != null){
            node.next = list1;
        }
        return pre.next;
    }

    static ListNode createListNode(String[] arr){
        if(arr == null || arr.length == 0){
            return null;
        }
        ListNode pre = new ListNode(0);
        ListNode stamp = pre;
        for(int i = 0; i < arr.length; i++){
            stamp.next = new ListNode(Integer.valueOf(arr[i]));
            stamp = stamp.next;
        }
        return pre.next;
    }



    static class ListNode{
        private int val;
        private ListNode next;

        public ListNode(){};

        public ListNode(int val){
            this.val = val;
        }
        public ListNode(int val, ListNode node){
            this.val = val;
            this.next = node;
        }
    }
}
