package com.nilin.algorithm.nowcoder;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class HJ10 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String a = in.nextLine();
            char[] arr = a.toCharArray();
            Set<String> set = new HashSet<>();
            for(int i = 0; i < arr.length; i++){
                if(arr[i] >= 0 && arr[i] <= 127){
                    set.add(String.valueOf(arr[i]));
                }
            }
            System.out.println(set.size());
        }
    }
}
