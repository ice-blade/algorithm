package com.nilin.algorithm.nowcoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 输入一个单向链表，输出该链表中倒数第k个结点，链表的倒数第1个结点为链表的尾指针。
 * 链表结点定义如下：
 * struct ListNode
 * {
 *     int m_nKey;
 *     ListNode* m_pNext;
 * };
 * 正常返回倒数第k个结点指针，异常返回空指针.
 * 要求：
 * (1)正序构建链表;
 * (2)构建后要忘记链表长度。
 * 数据范围：链表长度满足 1≤n≤1000， 链表中数据满足 0≤val≤10000
 *
 * 输入描述：
 * 输入说明
 * 1 输入链表结点个数
 * 2 输入链表的值
 * 3 输入k的值
 *
 * 输出描述：
 * 输出一个整数
 *
 * 输入：
 * 8
 * 1 2 3 4 5 6 7 8
 * 4
 * 输出：
 * 5
 */
public class HJ51 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            ListNode pre = new ListNode(0);
            ListNode node = pre;
            for(int i = 0; i < n; i++){
                node.next = new ListNode(in.nextInt());
                node = node.next;
            }
            int k = in.nextInt();
            ListNode listNode = pre.next;
            ListNode o = find(listNode, k);
            if(o != null){
                System.out.println(o.val);
            }else {
                System.out.println(0);
            }
        }
    }
    
    public static ListNode find(ListNode listNode, int k){
        ListNode slow = listNode;
        ListNode fast = listNode;
        for(int i = 0; i < k; i++){
            if(fast == null){
                return null;
            }
            fast = fast.next;
        }
        while (fast != null){
            fast = fast.next;
            slow = slow.next;
        }
        return slow;
    }

    public static class ListNode {
        private int val;
        private ListNode next;
        public ListNode(int val){
            this.val = val;
        }
    }
}
