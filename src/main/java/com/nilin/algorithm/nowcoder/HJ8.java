package com.nilin.algorithm.nowcoder;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class HJ8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int size = in.nextInt();
            //Integer已实现Comparable接口，所以new TreeMap<>()不用指定比较方法
            Map<Integer, Integer> map = new TreeMap<>();
            for(int i = 0; i < size; i++){
                int key = in.nextInt();
                int value = in.nextInt();
                if(map.containsKey(key)){
                    map.put(key, map.get(key) + value);
                }else{
                    map.put(key, value);
                }
            }
            for(Integer key : map.keySet()){
                System.out.println(key + " " + map.get(key));
            }
        }
    }
}
