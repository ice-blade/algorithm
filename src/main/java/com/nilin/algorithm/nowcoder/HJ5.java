package com.nilin.algorithm.nowcoder;

import java.util.Scanner;

/**
 * 写出一个程序，接受一个十六进制的数，输出该数值的十进制表示。
 * 1 2 3 4 5 6 7 8 9 a b c d e f
 */
public class HJ5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()){
            String line = in.nextLine();
            String num = line.substring(2);
            char[] arr = num.toCharArray();
            int i = get(arr);

            System.out.println(i);
        }
    }

    public static int get(char[] arr){
        int num = 0;
        int b = 0;
        for (int i = 0; i < arr.length; i++){
            int l = arr.length - i;
            char c = arr[i];
            if(c >= 'a' && c <= 'f'){
                b = 10 + c - 'a';
            } else if (c >= 'A' && c <= 'F') {
                b = 10 + c - 'A';
            }else {
                b = c - '0';
            }
            num += b * Math.pow(16, arr.length - i - 1);
        }
        return num;
    }


}
