package com.nilin.algorithm.nowcoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class HJ16 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int totalPrice = in.nextInt();
            int size = in.nextInt();
            List<Goods> list = new ArrayList<>(size);
            for(int i = 1; i <= size; i++){
                Goods g = new Goods();
                g.setId(i);
                g.setPrice(in.nextInt());
                g.setPos(in.nextInt());
                g.setDeg(g.getPrice() * g.getPos());
                g.setParentId(in.nextInt());
                list.add(g);
            }
            list = list.stream().sorted((o1, o2) -> o2.getDeg() - o1.getDeg()).collect(Collectors.toList());

            List<Cart> cartList = new ArrayList<>(size);
            for(int i = 1;i <= list.size(); i++){
                int sum = 0;
                int deg = 0;
                List<Goods> goodsList = new ArrayList<>();
                Cart cart = new Cart();
                Goods goods = list.get(i-1);
                sum += goods.getPrice();
                if(sum > totalPrice)
                    continue;
                Integer parentId = goods.getParentId();
                if(parentId > 0){
                    Goods parent = list.stream().filter(o -> o.getId().equals(parentId)).findFirst().get();
                    sum += parent.getPrice();
                    if(sum > totalPrice)
                       continue;
                    deg += parent.getPrice() * parent.getPos();
                    goodsList.add(parent);
                }
                deg += goods.getPrice() * goods.getPos();
                goodsList.add(goods);

                for (int j = 1;j <= list.size(); j++){
                    if(j == i)
                        continue;
                    Goods goods1 = list.get(j-1);
                    sum += goods1.getPrice();
                    if(sum > totalPrice){
                        sum -= goods1.getPrice();
                        continue;
                    }
                    Integer parentId1 = goods1.getParentId();
                    if(parentId1 > 0){
                        Goods parent1 = list.stream().filter(o -> o.getId().equals(parentId1)).findFirst().get();
                        boolean anyMatch = goodsList.stream().anyMatch(item -> item.getId().equals(parent1.getId()));
                        if(!anyMatch){
                            sum += parent1.getPrice();
                            if(sum > totalPrice){
                                sum -= parent1.getPrice();
                                sum -= goods1.getPrice();
                                continue;
                            }
                            deg += parent1.getPrice() * parent1.getPos();
                            goodsList.add(parent1);
                        }
                    }
                    deg += goods1.getPrice() * goods1.getPos();
                    goodsList.add(goods1);
                }
                cart.setList(goodsList);
                cart.setDeg(deg);
                cartList.add(cart);
            }
            Cart cart = cartList.stream().max((o1, o2) -> o1.getDeg() - o2.getDeg()).get();
            System.out.println(cart.getDeg());
        }
    }

    static class Cart{
        private List<Goods> list;
        private int deg = 0;

        public List<Goods> getList() {
            return list == null ? new ArrayList<>() : list;
        }

        public void setList(List<Goods> list) {
            this.list = list;
        }

        public int getDeg() {
            return deg;
        }

        public void setDeg(int deg) {
            this.deg = deg;
        }
    }

    static class Goods{
        private Integer id;

        private Integer parentId;

        private int pos;

        private int price;

        private int deg;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getParentId() {
            return parentId;
        }

        public void setParentId(Integer parentId) {
            this.parentId = parentId;
        }

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getDeg() {
            return deg;
        }

        public void setDeg(int deg) {
            this.deg = deg;
        }
    }
}
