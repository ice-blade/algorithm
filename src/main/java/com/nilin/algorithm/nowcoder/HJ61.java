package com.nilin.algorithm.nowcoder;

import java.util.Scanner;

/**
 * 把m个同样的苹果放在n个同样的盘子里，允许有的盘子空着不放，问共有多少种不同的分法？
 * 注意：如果有7个苹果和3个盘子，（5，1，1）和（1，5，1）被视为是同一种分法。
 * <p>
 * 数据范围：0≤m≤10，1≤n≤10 。
 * <p>
 * 6 * 6
 * 0 1 2 3 4 5 6
 * 1 1 1 1 1 1 1
 * 2 1 2 2 2 2 2
 * 3 1 2 3 3 3 3
 * 4 1 3 4 5 5 5
 * 5 1 3 5 6 7 7
 * 6 1 4 7 9 10 11
 */
public class HJ61 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int m = in.nextInt();
            int n = in.nextInt();
            int[][] arr = new int[m + 1][n + 1];
            for(int i=0;i<=m;i++){
                arr[i][0] = 1;
                arr[i][1] = 1;
            }
            for(int j=0;j<=n;j++){
                arr[0][j] = 1;
                arr[1][j] = 1;
            }
            for (int i = 2; i <= m; i++) {
                for (int j = 2; j <= n; j++) {
                    if (i >= j) {
                        arr[i][j] = arr[i][j - 1] + arr[i-j][j];
                    } else {
                        arr[i][j] = arr[i][j - 1];
                    }
                }
            }
            System.out.println(arr[m][n]);
        }
    }


}
