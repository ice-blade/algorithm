package com.nilin.algorithm.nowcoder;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 * 给定一些同学的信息（名字，成绩）序列，请你将他们的信息按照成绩从高到低或从低到高的排列,相同成绩
 * 都按先录入排列在前的规则处理。
 *
 * 例示：
 * jack      70
 * peter     96
 * Tom       70
 * smith     67
 *
 * 从高到低  成绩
 * peter     96
 * jack      70
 * Tom       70
 * smith     67
 *
 * 从低到高
 *
 * smith     67
 * jack      70
 * Tom       70
 * peter     96
 *
 * 注：0代表从高到低，1代表从低到高
 */
public class HJ68 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNext()) { // 注意 while 处理多个 case
            int a = Integer.valueOf(in.next());
            int b = Integer.valueOf(in.next());
            Student[] arr = new Student[a];
            for(int i = 0; i < a; i++){
                arr[i] = new Student(in.next(), Integer.valueOf(in.next()));
            }
            if(b == 0){
                Arrays.sort(arr, (o1,o2) -> o2.score - o1.score);
            }else {
                Arrays.sort(arr, (o1,o2) -> o1.score - o2.score);
            }
            for (int i = 0; i < a; i++){
                System.out.println(arr[i].name + " " + arr[i].score);
            }
        }
    }

    public static class Student{
        String name;
        int score;

        public Student(String name, int score){
            this.name = name;
            this.score = score;
        }
    }
}
