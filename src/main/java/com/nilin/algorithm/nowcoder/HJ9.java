package com.nilin.algorithm.nowcoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HJ9 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            String line = in.nextLine();
            char[] arr = line.toCharArray();
            List<String> list = new ArrayList<>();
            for(int i = arr.length - 1;i >= 0;i--){
                String s = String.valueOf(arr[i]);
                if(!list.contains(s)){
                    list.add(s);
                    System.out.printf(s);
                }
            }

        }
    }
}
