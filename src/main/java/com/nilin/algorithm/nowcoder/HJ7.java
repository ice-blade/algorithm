package com.nilin.algorithm.nowcoder;

import java.util.Scanner;

public class HJ7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextFloat()) { // 注意 while 处理多个 case
            String line = in.nextLine();
            String num = line.substring(0, line.indexOf("."));
            char n = line.charAt(line.indexOf(".") + 1);
            if(n >= '5'){
                System.out.println(Integer.valueOf(num) + 1);
            }else{
                System.out.println(Integer.valueOf(num));
            }
        }
    }
}
