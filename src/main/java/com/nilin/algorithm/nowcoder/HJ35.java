package com.nilin.algorithm.nowcoder;

import java.util.Scanner;

/**
 * 蛇形矩阵是由1开始的自然数依次排列成的一个矩阵上三角形。
 * 例如，当输入5时，应该输出的三角形为：
 * 1 3 6 10 15
 * 2 5 9 14
 * 4 8 13
 * 7 12
 * 11
 */
public class HJ35 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            int a = 1;
            for (int i = 0; i < n; i++) {
                a += i;
                int b = a;
                System.out.print(b + " ");
                for (int j = 0; j < n - i - 1; j++) {
                    b += i + j + 2;
                    System.out.print(b + " ");
                }
                System.out.println();
            }
        }
    }
}
