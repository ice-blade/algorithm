package com.nilin.algorithm.nowcoder;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 编写一个程序，将输入字符串中的字符按如下规则排序。
 * 规则 1 ：英文字母从 A 到 Z 排列，不区分大小写。
 * 如，输入： Type 输出： epTy
 * 规则 2 ：同一个英文字母的大小写同时存在时，按照输入顺序排列。
 * 如，输入： BabA 输出： aABb
 * 规则 3 ：非英文字母的其它字符保持原来的位置。
 * 如，输入： By?e 输出： Be?y
 */
public class HJ26 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String line = in.nextLine();
            char[] arr = line.toCharArray();
            List<Integer> list1 = new ArrayList<>();
            List<Character> list2 = new ArrayList<>();
            for(int i = 0; i < arr.length; i++){
                if((arr[i] >= 'a' && arr[i] <= 'z') || (arr[i] >= 'A' && arr[i] <= 'Z')){
                    list1.add(i);
                    list2.add(arr[i]);
                }
            }
            list2.sort((o1, o2) -> Character.toUpperCase(o1) - Character.toUpperCase(o2));
            for(int i = 0; i < list2.size(); i++){
                arr[list1.get(i)] = list2.get(i);
            }
            System.out.println(arr);
        }
    }
}
