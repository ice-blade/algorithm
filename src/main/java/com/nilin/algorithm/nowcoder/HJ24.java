package com.nilin.algorithm.nowcoder;

import java.util.Scanner;

/**
 * 输入：
 * 8
 * 186 186 150 200 160 130 197 200
 * 输出：
 * 4
 * 说明：
 * 由于不允许改变队列元素的先后顺序，所以最终剩下的队列应该为186 200 160 130或150 200 160 130
 */
public class HJ24 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = in.nextInt();
            }

            int[] left = new int[n];
            int[] right = new int[n];

            left[0] = 1;
            right[n - 1] = 1;

            for (int i = 1; i < n; i++) {
                left[i] = 1;
                for (int j = 0; j < i; j++) {
                    if (arr[j] < arr[i]) {
                        left[i] = Math.max(left[i], left[j] + 1);
                    }
                }
            }

            for (int i = n - 2; i >= 0; i--) {
                right[i] = 1;
                for (int j = n - 1; j > i; j--) {
                    if (arr[j] < arr[i]) {
                        right[i] = Math.max(right[i], right[j] + 1);
                    }
                }
            }

            int[] len = new int[n];
            int max = 0;
            for (int i = 1; i < n - 1; i++) {
                len[i] = left[i] + right[i] - 1;
                max = Math.max(max, len[i]);
            }
            System.out.println(n - max);
        }
    }
}
