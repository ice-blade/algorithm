package com.nilin.algorithm.nowcoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 有一种兔子，从出生后第3个月起每个月都生一只兔子，小兔子长到第三个月后每个月又生一只兔子。
 例子：假设一只兔子第3个月出生，那么它第5个月开始会每个月生一只兔子。
 一月的时候有一只兔子，假如兔子都不死，问第n个月的兔子总数为多少？
 数据范围：输入满足 1≤n≤31

 输入描述：
 输入一个int型整数表示第n个月

 输出描述：
 输出对应的兔子总数
 */
public class HJ37 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            R r = new R(0);
            List<R> list = new ArrayList<>();
            list.add(r);
            for(int i = 0; i < n; i++){
                List<R> list1 = new ArrayList<>();
                for(R item : list){
                    item.age = item.age + 1;
                    if(item.age >= 3){
                        list1.add(new R(1));
                    }
                }
                list.addAll(list1);
            }

            System.out.println(list.size());
        }
    }

    public static class R {
        private int age;

        public R(int age){
            this.age = age;
        }
    }
}
