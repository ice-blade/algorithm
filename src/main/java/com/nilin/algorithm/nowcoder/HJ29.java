package com.nilin.algorithm.nowcoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * 编写一个程序，将输入字符串中的字符按如下规则排序。
 * 规则 1 ：英文字母从 A 到 Z 排列，不区分大小写。
 * 如，输入： Type 输出： epTy
 * 规则 2 ：同一个英文字母的大小写同时存在时，按照输入顺序排列。
 * 如，输入： BabA 输出： aABb
 * 规则 3 ：非英文字母的其它字符保持原来的位置。
 * 如，输入： By?e 输出： Be?y
 */
public class HJ29 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String a = in.nextLine();
            String b = in.nextLine();
            System.out.println(encrypt(a));
            System.out.println(decrypt(b));
        }
    }

    public static String encrypt(String a){
        char[] arr = a.toCharArray();
        for(int i = 0; i < arr.length; i++){
            char c = arr[i];
            if(c >= '0' && c <= '9'){
                if(c == '9'){
                    arr[i] = '0';
                }else{
                    arr[i] = (char)(c + 1);
                }
            }
            if(c >= 'A' && c <= 'Z'){
                if(c == 'Z'){
                    arr[i] = 'a';
                }else{
                    arr[i] = (char)(c + 33);
                }
            }
            if(c >= 'a' && c <= 'z'){
                if(c == 'z'){
                    arr[i] = 'A';
                }else{
                    arr[i] = (char)(c - 31);
                }
            }
        }
        return new String(arr);
    }

    public static String decrypt(String a){
        char[] arr = a.toCharArray();
        for(int i = 0; i < arr.length; i++){
            char c = arr[i];
            if(c >= '0' && c <= '9'){
                if(c == '0'){
                    arr[i] = '9';
                }else{
                    arr[i] = (char)(c - 1);
                }
            }
            if(c >= 'A' && c <= 'Z'){
                if(c == 'A'){
                    arr[i] = 'z';
                }else{
                    arr[i] = (char)(c + 31);
                }
            }
            if(c >= 'a' && c <= 'z'){
                if(c == 'a'){
                    arr[i] = 'Z';
                }else{
                    arr[i] = (char)(c - 33);
                }
            }
        }
        return new String(arr);
    }
}
