package com.nilin.algorithm.nowcoder;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 输入：
 * 15 123 456 786 453 46 7 5 3 665 453456 745 456 786 453 123
 * 5 6 3 6 3 0
 * 输出：
 * 30 3 6 0 123 3 453 7 3 9 453456 13 453 14 123 6 7 1 456 2 786 4 46 8 665 9 453456 11 456 12 786
 * 说明：
 * 将序列R：5,6,3,6,3,0（第一个5表明后续有5个整数）排序去重后，可得0,3,6。
 * 序列I没有包含0的元素。
 * 序列I中包含3的元素有：I[0]的值为123、I[3]的值为453、I[7]的值为3、I[9]的值为453456、I[13]的值为453、I[14]的值为123。
 * 序列I中包含6的元素有：I[1]的值为456、I[2]的值为786、I[4]的值为46、I[8]的值为665、I[9]的值为453456、I[11]的值为456、I[12]的值为786。
 * 最后按题目要求的格式进行输出即可。
 */
public class HJ25 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int m = in.nextInt();
            List<Integer> I = new ArrayList<>(m);
            for (int i = 0; i < m; i++) {
                I.add(in.nextInt());
            }
            int n = in.nextInt();
            List<Integer> R = new ArrayList<>(n);
            for (int i = 0; i < n; i++) {
                R.add(in.nextInt());
            }
            R = R.stream().distinct().sorted().collect(Collectors.toList());
            List<Integer> list = new ArrayList<>();
            for (int i = 0; i < R.size(); i++) {
                Map<Integer, Integer> map = new LinkedHashMap<>();
                for (int j = 0; j < I.size(); j++) {
                    if (String.valueOf(I.get(j)).contains(String.valueOf(R.get(i)))) {
                        map.put(j, I.get(j));
                    }
                }
                if(map.size() > 0){
                    list.add(R.get(i));
                    list.add(map.size());
                    for(Integer key : map.keySet()){
                        list.add(key);
                        list.add(map.get(key));
                    }
                }
            }
            int size = list.size();
            list.add(0, size);
            for(int i = 0; i < list.size(); i++){
                if(i == list.size() - 1){
                    System.out.printf(String.valueOf(list.get(i)));
                }else {
                    System.out.printf(list.get(i) + ",");
                }
            }
        }
    }
}
