package com.nilin.algorithm.nowcoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * 编写一个程序，将输入字符串中的字符按如下规则排序。
 * 规则 1 ：英文字母从 A 到 Z 排列，不区分大小写。
 * 如，输入： Type 输出： epTy
 * 规则 2 ：同一个英文字母的大小写同时存在时，按照输入顺序排列。
 * 如，输入： BabA 输出： aABb
 * 规则 3 ：非英文字母的其它字符保持原来的位置。
 * 如，输入： By?e 输出： Be?y
 */
public class HJ27 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String line = in.nextLine();
            String[] source = line.split(" ");
            int n = Integer.valueOf(source[0]);
            String[] arr = new String[n];
            for (int i = 0; i < n; i++) {
                arr[i] = source[i + 1];
            }
            String x = source[n + 1];
            int k = Integer.valueOf(source[n + 2]);

            List<String> list = new ArrayList<>();
            list.add(x);
            char[] x1 = x.toCharArray();
            Arrays.sort(x1);
            for (int i = 0; i < n; i++) {
                String stamp = arr[i];
                if (x.length() != stamp.length()) {
                    continue;
                }
                char[] x2 = stamp.toCharArray();
                Arrays.sort(x2);
                if(!new String(x1).equals(new String(x2))){
                    continue;
                }
                if (!list.contains(stamp)) {
                    list.add(stamp);
                }
            }
            list = list.stream().filter(o -> !o.equals(x)).sorted().collect(Collectors.toList());
            System.out.println(list.size());
            if (k <= list.size()) {
                System.out.println(list.get(k - 1));
            }
        }
    }
}
