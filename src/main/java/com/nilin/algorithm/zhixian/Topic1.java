package com.nilin.algorithm.zhixian;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Topic1 {

    public static void main(String[] args) {
        Map<String, String> student = new HashMap<>();
        student.put("张三", "计算机");
        student.put("李四", "英语");
        student.put("王五", "数学");
        student.put("赵六", "文学");

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String key = scanner.nextLine();
            if (student.containsKey(key)) {
                //工厂获取实现类
                Field field = Factory.getField(student.get(key));
                System.out.println(student.get(key) + "," + field.teacherName() + "," + field.classRoom());
                field.study();
            }
        }
    }

    static class Factory {
        static Map<String, Field> pool = new HashMap<>();

        static {
            pool.put("计算机", new Computer());
            pool.put("英语", new English());
            pool.put("数学", new Math());
            pool.put("文学", new Literature());
        }

        static Field getField(String key) {
            return pool.get(key);
        }
    }

    interface Field {

        //获取专业课老师
        public String teacherName();

        //获取教室地点
        public String classRoom();

        //专业课操作
        public void study();
    }

    //计算机
    static class Computer implements Field {

        @Override
        public String teacherName() {
            return "张四";
        }

        @Override
        public String classRoom() {
            return "A";
        }

        @Override
        public void study() {
            //随机生成2个10以内的数字m，n，并计算 m * 2 ^ n 的结果，打印出来
            Random random = new Random();
            int m = random.nextInt(10);
            int n = random.nextInt(10);
            int r = m * (2 >> n - 1);
            System.out.println(r);
        }
    }

    //英语
    static class English implements Field {
        @Override
        public String teacherName() {
            return "李五";
        }

        @Override
        public String classRoom() {
            return "B";
        }

        @Override
        public void study() {
            //随机一个小于26的数字m，并随机输出m个不重复的小写英文字⺟组成的字符串
            Random random = new Random();
            int m = random.nextInt(26);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < m; i++) {
                //随机一个小写字母：
                char c = (char) (random.nextInt(26) + 'a');
                if (sb.indexOf(String.valueOf(c)) == -1) {
                    sb.append(c);
                } else {
                    i--;
                }
            }
            System.out.println(sb);
        }
    }

    //数学
    static class Math implements Field {

        @Override
        public String teacherName() {
            return "王六";
        }

        @Override
        public String classRoom() {
            return "C";
        }

        @Override
        public void study() {
            //随机一个3位的正整数m，并反转输出
            Random random = new Random();
            int m = random.nextInt(900) + 100;
            String str = Integer.toString(m);
            StringBuilder sb = new StringBuilder(str);
            //反转字符串
            sb.reverse();
            System.out.println(sb);
        }
    }

    //文学
    static class Literature implements Field {

        @Override
        public String teacherName() {
            return "赵七";
        }

        @Override
        public String classRoom() {
            return "D";
        }

        @Override
        public void study() {
            //随机输出3个汉字
            System.out.println();
        }
    }
}
