package com.nilin.algorithm.zhixian;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Topic2 {


    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()){
            //输入m个月后
            int m = scanner.nextInt();
            //输入兔子性成熟月数
            int n = scanner.nextInt();
            System.out.println(getTotal(m,n));
        }
    }

    static class Rabbit {
        public Rabbit(int month) {
            this.month = month;
        }
        //兔子的月龄
        private int month;

    }

    /**
     * 获取兔子数量
     * @param m 经过m个月后
     * @param n 兔子性成熟月数
     * @return
     */
    static int getTotal(int m, int n) {
        //已成熟兔子数量
        int sum = 0;
        //未成熟兔子集合
        List<Rabbit> list = new ArrayList<>();
        //初始兔子
        list.add(new Rabbit(1));
        while (m > 0) {
            //每月成熟兔子新生育兔子
            for (int i = 0; i < sum; i++) {
                list.add(new Rabbit(0));
            }
            for (int j = 0; j < list.size(); j++) {
                list.get(j).month++;
                //如果未成熟兔子中月龄有大于性成熟月数的
                if (list.get(j).month >= n) {
                    sum++;
                    list.remove(j);
                    j--;
                }
            }
            m--;
        }
        return sum + list.size();
    }
}
