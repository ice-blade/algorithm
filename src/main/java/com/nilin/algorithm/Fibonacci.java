package com.nilin.algorithm;

public class Fibonacci {

    public static void main(String[] args) {
        int sum = jumpFloor(0);
        System.out.println(sum);
    }

    /**
     * 斐波那契数列
     * 0,1,1,2,3,5,8,13,21,34,55,...
     *
     * 迭代法
     */
    static int fibonacci1(int number) {
        if (number <= 0) {
            return 0;
        }
        if (number == 1) {
            return 1;
        }
        int first = 0, second = 1, res = 0;
        for (int i = 2; i <= number; i++) {
            res = first + second;
            first = second;
            second = res;
        }
        return res;
    }

    /**
     * 斐波那契数列
     * 0,1,1,2,3,5,8,13,21,34,55,...
     *
     * 递归法
     */
    public static int fibonacci2(int n) {
        if (n <= 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }

        return fibonacci2(n - 2) + fibonacci2(n - 1);
    }

    /**
     * 跳台阶问题：一只青蛙一次可以跳上 1 级台阶，也可以跳上 2 级。求该青蛙跳上一个 n 级的台阶总共有多少种跳法
     * 问题分析：
     *  a.如果两种跳法，1 阶或者 2 阶，那么假定第一次跳的是一阶，那么剩下的是 n-1 个台阶，跳法是 f(n-1);
     *  b.假定第一次跳的是 2 阶，那么剩下的是 n-2 个台阶，跳法是 f(n-2)
     *  c.由 a，b 假设可以得出总跳法为: f(n) = f(n-1) + f(n-2)
     *  d.然后通过实际的情况可以得出：只有一阶的时候 f(1) = 1 ,只有两阶的时候可以有 f(2) = 2
     *
     * 这道题其实就是变化后的斐波那契数列的问题。不同的是本来的斐波那契数列是0,1,1,2,3,5,8,...
     * 变化后是1,2,3,5,8,13,21,34,55,...
     */
    static int jumpFloor(int number) {
        if (number <= 0) {
            return 0;
        }
        if (number == 1) {
            return 1;
        }
        if (number == 2) {
            return 2;
        }
        int first = 1, second = 2, res = 0;
        for (int i = 3; i <= number; i++) {
            res = first + second;
            first = second;
            second = res;
        }
        return res;
    }



}
