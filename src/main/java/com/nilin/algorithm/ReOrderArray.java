package com.nilin.algorithm;

import java.util.Arrays;

public class ReOrderArray {
    public static void main(String[] args) {
        int i = 3 & 1;
        int j = 4 & 1;
        int k = 5 & 1;
        int l = 6 & 1;
        int[] arr = {1,2,3,4,5,6,7,8};
        reOrderArray(arr);
    }

    public static void reOrderArray(int[] array) {
        //如果数组长度等于0或者等于1，什么都不做直接返回
        if (array.length == 0 || array.length == 1)
            return;
        //oddCount：保存奇数个数
        //oddBegin：奇数从数组头部开始添加
        int oddCount = 0, oddBegin = 0;
        //新建一个数组
        int[] newArray = new int[array.length];
        //计算出（数组中的奇数个数）开始添加元素
        for (int i = 0; i < array.length; i++) {
            if ((array[i] & 1) == 1) oddCount++;
        }
        for (int i = 0; i < array.length; i++) {
            //如果数为基数新数组从头开始添加元素
            //如果为偶数就从oddCount（数组中的奇数个数）开始添加元素
            if ((array[i] & 1) == 1)
                newArray[oddBegin++] = array[i];
            else newArray[oddCount++] = array[i];
        }
        for (int i = 0; i < array.length; i++) {
            array[i] = newArray[i];
        }
    }

}
