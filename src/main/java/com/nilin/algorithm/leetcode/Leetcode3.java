package com.nilin.algorithm.leetcode;

import java.util.HashMap;
import java.util.Map;

public class Leetcode3 {

    public static void main(String[] args) {
        String s = "abC124A32Cb3";
        int n = lengthOfLongestOnlyOneLetter(s);
        System.out.println(n);
    }

    public static int lengthOfLongestOnlyOneLetter(String s) {
        int numCount = 0;
        int charCount = 0;
        int left = 0;
        int right = 0;
        int max = -1;
        while (right < s.length()){
            if(isChar(s.charAt(right))){
                charCount++;
            }else {
                numCount++;
            }

            if(charCount == 1 && numCount > 0){
                max = Math.max(max, right - left + 1);
            }

            while (charCount > 1){
                if(isChar(s.charAt(left))){
                    charCount--;
                }else {
                    numCount--;
                }
                left++;
            }
            right++;
        }
        return max;
    }

    public static int lengthOfLongestSubstring(String s) {
        if(s.length() == 0) return 0;
        Map<Character, Integer> map = new HashMap<>();
        int left = 0;
        int max = 0;
        for(int i = 0; i < s.length(); i++){
            if(map.containsKey(s.charAt(i))){
                left = Math.max(left, map.get(s.charAt(i)) + 1);
            }
            map.put(s.charAt(i), i);
            max = Math.max(max, i - left + 1);
        }
        return max;
    }



    static boolean isChar(char s){
        return (s >= 'a' && s <= 'z') || (s >= 'A' && s <= 'Z');
    }

    static boolean isNum(char s){
        return s >= '0' && s <= '9';
    }



}
