package com.nilin.algorithm.leetcode;

/**
 * 42. 接雨水
 * 给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水。
 */
public class Leetcode42 {

    public int trap(int[] height) {
        int n = height.length;
        if(n <= 2) return 0;
        //定义数组left,存储height数组每个元素左侧最大值
        int[] left = new int[n];
        left[0] = height[0];
        for(int i = 1; i < n; i++){
            left[i] = Math.max(left[i-1], height[i]);
        }
        //定义数组right,存储height数组每个元素右侧最大值
        int[] right = new int[n];
        right[n-1] = height[n-1];
        for(int i = n-2; i >= 0; i--){
            right[i] = Math.max(right[i+1], height[i]);
        }
        //height数组每个元素处能接的雨水：
        int sum = 0;
        for(int i = 0; i < n; i++){
            sum += Math.min(left[i], right[i]) - height[i];
        }
        return sum;
    }

    public int batter(int[] height) {
        int n = height.length;
        int left = 0, right = n - 1, leftMax = 0, rightMax = 0;
        int sum = 0;
        while (left < right){
            if(height[left] < height[right]){
                leftMax = Math.max(leftMax, height[left]);
                sum += leftMax - height[left];
                left++;
            }else{
                rightMax = Math.max(rightMax, height[right]);
                sum += rightMax - height[right];
                right--;
            }
        }
        return sum;
    }
}
