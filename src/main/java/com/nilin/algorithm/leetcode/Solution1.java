package com.nilin.algorithm.leetcode;

public class Solution1 {

    public static void main(String[] args) {
        int[] a = {0,0,0,0,1,1,1,2,3,3};
        int[] b = {2, 5, 6};
        removeDuplicates(a);
    }

    public static int majorityElement(int[] nums) {
        int m = nums[0];
        int n = 0;
        for(int i = 0;i < nums.length;i++){
            if(n == 0){
                m = nums[i];
            }
            n += m == nums[i] ? 1 : -1;
        }
        return m;
    }

    public static int removeDuplicates(int[] nums) {
        int n = nums.length;
        if (n == 0) {
            return 0;
        }
        int slow = 1, fast = 1;
        while (fast < n) {
            if (nums[slow - 1] != nums[fast]) {
                nums[slow] = nums[fast];
                ++slow;
            }
            ++fast;
        }
        return slow;
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        for (int i = 0; i < n; i++) {
            nums1[m + i] = nums2[i];
        }
        for (int j = 1; j < nums1.length; j++) {
            for (int k = 0; k < nums1.length - j; k++) {
                if (nums1[k] > nums1[k + 1]) {
                    int temp = nums1[k + 1];
                    nums1[k + 1] = nums1[k];
                    nums1[k] = temp;
                }
            }
        }
    }


}
