package com.nilin.algorithm.leetcode;

public class Leetcode11 {

    public int maxArea(int[] height) {
        int area = 0;
        for (int i = 0; i < height.length; i++) {
            for (int j = 0; j < height.length; j++) {
                if (i == j)
                    continue;
                area = Math.max(area, Math.abs(i - j) * Math.min(height[i], height[j]));
            }
        }
        return area;
    }

    public int better(int[] height) {
        int len = height.length;
        if(len == 0) return 0;
        int area = 0;
        int left = 0, right = len -1;

        while (left < right){
            area = Math.max(area, (right - left) * Math.min(height[left], height[right]));
            if(height[left] > height[right]){
                right--;
            }else {
                left++;
            }
        }
        return area;
    }
}
