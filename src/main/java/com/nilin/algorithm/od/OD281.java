package com.nilin.algorithm.od;

import java.util.*;

/**
 * 题目描述
 * 请在一个字符串中找出连续最长的数字串，并返回这个数字串。
 * 如果存在长度相同的连续数字串，返回最后一个。
 * 如果没有符合条件的字符串，返回空字符串””。
 * 注意：
 * 数字串可以由数字”0-9″、小数点”.”、正负号”±”组成，长度包括组成数字串的所有符号。
 * “.”、“±”仅能出现一次，”.”的两边必须是数字，”±”仅能出现在开头且其后必须要有数字。
 * 长度不定，可能含有空格。
 *
 * 输入
 * 1234567890abcd9.+12345.678.9ed
 * 输出
 * +12345.678
 */
public class OD281 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()){

            String line = in.nextLine();

            String s = get(line);

            System.out.println(s);
        }
    }

    public static String get(String s){

        return "NULL";
    }




}
