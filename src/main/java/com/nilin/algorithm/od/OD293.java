package com.nilin.algorithm.od;

import java.util.*;

/**
 * 题目描述
 * 所谓水仙花数，是指一个n位的正整数，其各位数字的n次方和等于该数本身。
 * 例如153是水仙花数，153是一个3位数，并且153 = 1^3 + 5^3 + 3^3。
 * 输入描述
 * 第一行输入一个整数n，表示一个n位的正整数。n在3到7之间，包含3和7。
 * 第二行输入一个整数m，表示需要返回第m个水仙花数。
 * 输出描述
 * 返回长度是n的第m个水仙花数。个数从0开始编号。
 * 若m大于水仙花数的个数，返回最后一个水仙花数和m的乘积。
 * 若输入不合法，返回-1。
 *
 * 输入
 * 3
 * 0
 * 输出
 * 153
 * 说明
 * 153是第一个水仙花数
 *
 * 输入
 * 9
 * 1
 * 输出
 * -1
 * 说明
 * 9超出范围
 */
public class OD293 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()){
            int n = in.nextInt();
            int m = in.nextInt();
            if(n < 3 || n > 7){
                System.out.println("-1");
                System.exit(0);
            }
            String start = "1";
            for(int i = 1; i < n; i++){
                start += "0";
            }
            String end = "9";
            for(int i = 1; i < n; i++){
                end += "9";
            }
            List<Integer> list = new ArrayList<>();
            for(int i = Integer.valueOf(start); i < Integer.valueOf(end); i++){
                String s = String.valueOf(i);
                int sum = 0;
                for(int j = 0; j < n; j++){
                    int temp = Integer.valueOf(String.valueOf(s.charAt(j)));
                    sum += Math.pow(temp, n);
                }
                if(i == sum){
                    list.add( i);
                }
            }
            if(list.size() <= m){
                m = list.size() - 1;
            }
            if(null != list.get(m)){
                System.out.println(list.get(m));
            }else {
                System.out.println("-1");
            }
        }
    }




}
