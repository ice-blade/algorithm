package com.nilin.algorithm.od;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * OD233. 可以组成网络的服务器
 * <p>
 * 题目描述
 * 在一个机房中，服务器的位置标识在 n*m 的整数矩阵网格中，1 表示单元格上有服务器，0 表示没有。如果两台服务器位于同一行或者同一列中紧邻的位置，则认为它们之间可以组成一个局域网。
 * 请你统计机房中最大的局域网包含的服务器个数。
 * <p>
 * 输入描述
 * 第一行输入两个正整数，n和m，0<n,m<=100
 * 之后为n*m的二维数组，代表服务器信息
 * <p>
 * 输出描述
 * 最大局域网包含的服务器个数。
 * <p>
 * 输入
 * 2 2
 * 1 0
 * 1 1
 * 输出
 * 3
 * 说明
 * [0][0]、[1][0]、[1][1]三台服务器相互连接，可以组成局域网
 */
public class OD233 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            int m = in.nextInt();
            int a = 0, b = 0;
            int[][] arr = new int[n][m];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    arr[i][j] = in.nextInt();
                    if (arr[i][j] == 1) {
                        a = i;
                        b = j;
                    }
                }
            }
            List<P> list = new ArrayList<>();
            get(arr, n, m, a, b, list);
            System.out.println(list.size());
        }
    }

    static void get(int[][] arr, int n, int m, int i, int j, List<P> list) {
        arr[i][j] = 0;
        list.add(new P(i, j));
        //上
        if (i >= 1 && arr[i - 1][j] == 1) {
            arr[i - 1][j] = 0;
            i--;
            get(arr, n, m, i, j, list);
        }
        //下
        if (i <= n - 2 && arr[i + 1][j] == 1) {
            arr[i + 1][j] = 0;
            i++;
            get(arr, n, m, i, j, list);
        }
        //左
        if (j >= 1 && arr[i][j - 1] == 1) {
            arr[i][j - 1] = 0;
            j--;
            get(arr, n, m, i, j, list);
        }
        //右
        if (j <= m - 2 && arr[i][j + 1] == 1) {
            arr[i][j + 1] = 0;
            j++;
            get(arr, n, m, i, j, list);
        }

    }

    static class P {
        int x;
        int y;

        public P(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
