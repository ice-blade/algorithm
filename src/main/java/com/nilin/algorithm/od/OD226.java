package com.nilin.algorithm.od;

import java.util.Arrays;
import java.util.Scanner;

/**
 * OD226. 求最多可以派出多少支团队
 * <p>
 * 题目描述
 * 用数组代表每个人的能力，一个比赛活动要求参赛团队的最低能力值为N，每个团队可以由1人或者2人组成，且1个人只能参加1个团队，计算出最多可以派出多少只符合要求的团队。
 * <p>
 * 输入描述
 * 第一行代表总人数，范围1-500000
 * 第二行数组代表每个人的能力
 * 数组大小，范围1-500000
 * 元素取值，范围1-500000
 * 第三行数值为团队要求的最低能力值，范围1-500000
 * 输出描述
 * 最多可以派出的团队数量
 * <p>
 * 输入
 * 5
 * 3 1 5 7 9
 * 8
 * 输出
 * 3
 * 说明
 * 说明 3、5组成一队 1、7一队 9自己一队 输出3
 * <p>
 * 输入
 * 7
 * 3 1 5 7 9 2 6
 * 8
 * 输出
 * 4
 * 说明
 * 3、5组成一队，1、7一队，9自己一队，2、6一队，输出4
 * <p>
 * 输入
 * 3
 * 1 1 9
 * 8
 * 输出
 * 1
 * 说明
 * 9自己一队，输出1
 */
public class OD226 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = in.nextInt();
            }
            int tamp = in.nextInt();
            int count = 0, left = 0, right = arr.length - 1;
            if (arr.length == 1) {
                if (arr[0] >= tamp) {
                    count++;
                }
            }
            Arrays.sort(arr);
            while (left < right) {
                if (arr[right] >= tamp) {
                    count++;
                    right--;
                    continue;
                }
                if (arr[left] + arr[right] >= tamp) {
                    count++;
                    left++;
                    right--;
                } else {
                    left++;
                }
            }
            System.out.println(count);
        }
    }
}
