package com.nilin.algorithm.od;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 题目描述
 * 输入一个由随机数组成的数列（数列中每个数均是大于 0 的整数，长度已知），和初始计数值 m。
 * 从数列首位置开始计数，计数到 m 后，将数列该位置数值替换计数值 m，
 * 并将数列该位置数值出列，然后从下一位置从新开始计数，直到数列所有数值出列为止。
 * 如果计数到达数列尾段，则返回数列首位置继续计数。
 * 请编程实现上述计数过程，同时输出数值出列的顺序。
 * 比如：输入的随机数列为：3,1,2,4，初始计数值 m=7，从数列首位置开始计数（数值 3 所在位置）
 * 第一轮计数出列数字为 2，计数值更新 m=2，出列后数列为 3,1,4，从数值 4 所在位置从新开始计数
 * 第二轮计数出列数字为 3，计数值更新 m=3，出列后数列为 1,4，从数值 1 所在位置开始计数
 * 第三轮计数出列数字为 1，计数值更新 m=1，出列后数列为 4，从数值 4 所在位置开始计数
 * 最后一轮计数出列数字为 4，计数过程完成。输出数值出列顺序为：2,3,1,4。
 * <p>
 * 输入描述
 * 第一行是初始数列intput_array
 * 第二行是初始数列的长度len
 * 第三行是初始计数值m
 * 输出描述
 * 数值出列顺序
 * <p>
 * 输入
 * 3,1,2,4
 * 4
 * 7
 * 输出
 * 2,3,1,4
 */
public class OD294 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String s = in.nextLine();
            String[] split = s.split(",");
            int len = Integer.valueOf(in.nextLine());
            int m = Integer.valueOf(in.nextLine());
            int[] input_array = new int[len];
            int[] output_array = new int[len];
            for (int i = 0; i < len; i++) {
                input_array[i] = Integer.valueOf(split[i]);
            }
            array_iterate(len, input_array, m, output_array);
            for (int i = 0; i < len; i++) {
                if (i < len - 1) {
                    System.out.print(output_array[i] + ",");
                } else {
                    System.out.print(output_array[i]);
                }
            }
        }
    }

    static void array_iterate(int len, int[] input_array, int m, int[] output_array) {
        int j = 0;
        for (int i = 0; i < len; i++) {
            if (m > len) {
                m -= len;
            }
            //计数次数
            int n = m;
            while (n > 0) {
                if (input_array[j] > 0) {
                    if (n == 1) {
                        output_array[i] = input_array[j];
                        m = input_array[j];
                        input_array[j] = 0;
                    }
                    n--;
                }
                j++;
                if (j > len - 1) {
                    j = 0;
                }
            }
        }
    }


}
