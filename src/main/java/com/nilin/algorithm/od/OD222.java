package com.nilin.algorithm.od;

import java.util.Scanner;

/**
 * 题目描述
 * RSA加密算法在网络安全世界中无处不在，它利用了极大整数因数分解的困难度，数据越大，安全系数越高，给定一个 32 位正整数，请对其进行因数分解，找出是哪两个素数的乘积。
 * 输入描述
 * 一个正整数 num， 0 < num < 2147483647
 * 输出描述
 * 如果成功找到，以单个空格分割，从小到大输出两个素数，分解失败，请输出-1, -1
 *
 * 输入
 * 15
 * 输出
 * 3 5
 *
 * 输入
 * 27
 * 输出
 * -1 -1
 */
public class OD222 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int a = in.nextInt();
            int count = 0;
            int m = 2;
            int n;
            while (m <= Math.sqrt(a)){
                if(a % m == 0) {
                    n = a / m;
                    if(isPrime(m) && isPrime(n)){
                        count++;
                        System.out.println(m + " " + n);
                    }
                }
                m++;
            }
            if(count == 0){
                System.out.println("-1 -1");
            }

        }
    }

    
    //判断一个数是否为素数
    static boolean isPrime(int num){
        int n = 2;
        while (n <= Math.sqrt(num)){
            if(num % n != 0){
                n++;
            }else {
                break;
            }
        }
        return n > Math.sqrt(num);
    }


}
