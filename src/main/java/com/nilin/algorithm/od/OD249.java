package com.nilin.algorithm.od;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 题目描述
 * 如果3个正整数(a,b,c)满足a^2 + b^2 = c^2的关系，则称(a,b,c)为勾股数（著名的勾三股四弦五），
 * 为了探索勾股数的规律，我们定义如果勾股数(a,b,c)之间两两互质（即a与b，a与c，b与c之间均互质，没有公约数），则其为勾股数元组（例如(3,4,5)是勾股数元组，(6,8,10)则不是勾股数元组）。
 * 请求出给定范围[N,M]内，所有的勾股数元组。
 * 输入描述
 * 起始范围N，1 <= N <= 10000
 * 结束范围M，N < M <= 10000
 * 输出描述
 * a,b,c请保证a < b < c,输出格式：a b c；
 * 多组勾股数元组请按照a升序，b升序，最后c升序的方式排序输出；
 * 给定范围中如果找不到勾股数元组时，输出”NA“。
 *
 * 输入
 * 1
 * 20
 * 输出
 * 3 4 5
 * 5 12 13
 * 8 15 17
 * 说明
 * [1,20]范围内勾股数有：(3 4 5)，(5 12 13)，(6 8 10)，(8 15 17)，(9 12 15)，(12 16 20)；
 * 其中，满足(a,b,c)之间两两互质的勾股数元组有：(3 4 5)，(5 12 13)，(8 15 17);
 * 按输出描述中顺序要求输出结果。
 *
 * 输入
 * 5
 * 10
 * 输出
 * NA
 * 说明
 * [5,10]范围内勾股数有：(6 8 10)；
 * 其中，没有满足(a,b,c)之间两两互质的勾股数元组；
 * 给定范围中找不到勾股数元组，输出”NA“
 */
public class OD249 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int m = in.nextInt();
            int n = in.nextInt();

            System.out.println();
        }
    }


    //判断两个数是否互为质数
    static boolean isCoprime(int n, int m){
        int t = 0;
        while (m > 0){
            t = n % m;
            n = m;
            m = t;
        }
        return n == 1;
    }


}
