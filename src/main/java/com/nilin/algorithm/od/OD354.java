package com.nilin.algorithm.od;

import java.util.*;

/**
 * OD354. 爱吃蟠桃的孙悟空
 * 题目描述
 * 孙悟空爱吃蟠桃，有一天趁着蟠桃园守卫不在来偷吃。已知蟠桃园有 N 棵桃树，每颗树上都有桃子，守卫将在 H 小时后回来。
 * 孙悟空可以决定他吃蟠桃的速度K（个/小时），每个小时选一颗桃树，并从树上吃掉 K 个，如果树上的桃子少于 K 个，则全部吃掉，并且这一小时剩余的时间里不再吃桃。
 * 孙悟空喜欢慢慢吃，但又想在守卫回来前吃完桃子。
 * 请返回孙悟空可以在 H 小时内吃掉所有桃子的最小速度 K（K为整数）。如果以任何速度都吃不完所有桃子，则返回0。
 *
 * 输入描述
 * 第一行输入为 N 个数字，N 表示桃树的数量，这 N 个数字表示每颗桃树上蟠桃的数量。
 * 第二行输入为一个数字，表示守卫离开的时间 H。
 * 其中数字通过空格分割，N、H为正整数，每颗树上都有蟠桃，且 0 < N < 10000，0 < H < 10000。
 *
 * 输出描述
 * 吃掉所有蟠桃的最小速度 K，无解或输入异常时输出 0。
 *
 * 输入
 * 2 3 4 5
 * 4
 * 输出
 * 5
 *
 * 输入
 * 2 3 4 5
 * 3
 * 输出
 * 0
 *
 * 输入
 * 30 11 23 4 20
 * 6
 * 输出
 * 23
 */
public class OD354 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String line = in.nextLine();
            String[] strArr = line.split(" ");
            List<Integer> list = new ArrayList<>();
            for(int i = 0; i < strArr.length; i++){
                list.add(Integer.valueOf(strArr[i]));
            }
            Collections.sort(list);
            int h = Integer.valueOf(in.nextLine());
            if(h < list.size()){
                System.out.println(0);
            }
            if(h == list.size()){
                System.out.println(getMax(list));
            }
            if(h > list.size()){
                while (h != list.size()){
                    Integer tamp = list.remove(list.size() - 1);
                    int l = tamp / 2;
                    int r = tamp - l;
                    list.add(l);
                    list.add(r);
                }
                System.out.println(getMax(list));
            }
        }
    }

    static int getMax(List<Integer> list){
        int max = 0;
        for(Integer i : list){
            max = Math.max(max, i);
        }
        return max;
    }

}
