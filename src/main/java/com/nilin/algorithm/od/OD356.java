package com.nilin.algorithm.od;

import java.util.Scanner;

/**
 * OD356. 分割均衡字符串
 * <p>
 * 题目描述
 * 均衡串定义：字符串中只包含两种字符，且这两种字符的个数相同。
 * 给定一个均衡字符串，请给出可分割成新的均衡子串的最大个数。
 * 约定：字符串中只包含大写的 X 和 Y 两种字符。
 * <p>
 * 输入描述
 * 输入一个均衡串。
 * 字符串的长度：[2， 10000]。
 * 给定的字符串均为均衡字符串
 * <p>
 * 输出描述
 * 输出可分割成新的均衡子串的最大个数。
 * <p>
 * 备注
 * 分割后的子串，是原字符串的连续子串
 * <p>
 * 输入
 * XXYYXY
 * 输出
 * 2
 * 说明
 * XXYYXY可分割为2个均衡子串，分别为：XXYY、XY
 */
public class OD356 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String line = in.nextLine();
            int left = 0, right = 2, count = 0;
            while (right <= line.length()) {
                String s = line.substring(left, right);
                if (isBalance(s)) {
                    count++;
                    left = right;
                }
                right++;
            }
            System.out.println(count);
        }
    }

    /**
     * @param line
     * @return
     */
    static boolean isBalance(String line) {
        char[] arr = line.toCharArray();
        int a = 0, b = 0;
        for (int i = 0; i < line.length(); i++) {
            if (arr[i] == 'X') {
                a++;
            }
            if (arr[i] == 'Y') {
                b++;
            }
        }
        return a == b;
    }
}
