package com.nilin.algorithm.od;

import java.util.Scanner;

/**
 * 题目描述
 * 开头和结尾都是元音字母（aeiouAEIOU）的字符串为元音字符串，其中混杂的非元音字母数量为其瑕疵度。比如:
 * “a” 、 “aa”是元音字符串，其瑕疵度都为0
 * “aiur”不是元音字符串（结尾不是元音字符）
 * “abira”是元音字符串，其瑕疵度为2
 * 给定一个字符串，请找出指定瑕疵度的最长元音字符子串，并输出其长度，如果找不到满足条件的元音字符子串，输出0。
 * 子串：字符串中任意个连续的字符组成的子序列称为该字符串的子串。
 *
 * 输入描述
 * 首行输入是一个整数，表示预期的瑕疵度flaw，取值范围[0, 65535]。
 * 接下来一行是一个仅由字符a-z和A-Z组成的字符串，字符串长度(0, 65535]。
 *
 * 输出描述
 * 输出为一个整数，代表满足条件的元音字符子串的长度。
 *
 * 输入
 * 0
 * asdbuiodevauufgh
 *
 * 输出
 * 3
 */
public class OD265 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()){
            int k = Integer.valueOf(in.nextLine());
            String line = in.nextLine();
            int i = getVowelSubstring(line, k);
            System.out.println(i);
        }
    }

    public static int getVowelSubstring(String s, int k){
        int max = 0;
        int left = 0, right = 0, flawCount = 0;
        for(int i = 0; i < s.length(); i++){
            if(isVowel(s.charAt(i))){
                left = i;
                right = left;
                break;
            }
        }
        while (right < s.length()){
            if(!isVowel(s.charAt(right))){
                flawCount++;
            }
            if(isVowel(s.charAt(right)) && flawCount == k){
                max = Math.max(max, right - left + 1);
            }
            while (left < s.length() - 1 && (flawCount > k || !isVowel(s.charAt(left)))){
                left++;
                if(!isVowel(s.charAt(left))){
                    flawCount--;
                }
            }
            right++;
        }
        return max;
    }

    public static boolean isVowel(char c){
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }
}
