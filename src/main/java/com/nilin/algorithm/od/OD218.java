package com.nilin.algorithm.od;

import java.util.Arrays;
import java.util.Scanner;

public class OD218 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()){
            String s = in.nextLine();
            int k = Integer.valueOf(in.nextLine());
            char[] arr = s.toCharArray();
            Arrays.sort(arr);
            if(k > s.length()) k = s.length();
            char m = arr[k - 1];
            int index = s.indexOf(m);
            System.out.println(index);
        }
    }
}
